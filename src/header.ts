namespace amd {
   let modules: {[name: string]: () => any} = {};

   export function define(
      name: string, deps: string[],
      callback: (...deps: any[]) => void
   ) {
      modules[name] = () => {
         modules[name] = () => {
            throw new Error(`Recursive require: ${name}`);
         }

         console.group(`Loading module: ${name}`);
         let exports = {};
         callback.apply(undefined, deps.map(name => {
            switch (name) {
               case 'require':
                  return require;
               case 'exports':
                  return exports;
               default:
                  return require(name);
            }
         }));

         console.groupEnd();
         modules[name] = () => {
            console.info(`Using cached module: ${name}`);
            return exports;
         };
         return exports;
      }
   }

   export function require(name: string): any {
      if (typeof modules[name] === 'undefined') {
         throw new Error(`Undefined module '${name}'`);
      } else {
         return modules[name]();
      }
   }
}
let define = amd.define;