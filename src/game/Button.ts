import ctx from '../util/ctx';
import font from '../util/font';
import MouseArea from '../util/MouseArea';
import Scene from '../scene/Scene';

export default class Button extends MouseArea {
   font: string;
   active = false;

   constructor(
      x: number, y: number,
      private text: string,  private callback: () => void
   ) {
      super(x, y, 0, 50);
      
      this.font = `25px ${font.name}`;
      ctx.font = this.font;
      let metrics = ctx.measureText(text);
      this.width = metrics.width + 50;
   }

   mouseDown() {
      this.active = true;
      Scene.requestDraw();
   }

   mouseMove(event: MouseEvent) {
      let active = this.mouseInside(event);
      if (this.active !== active) {
         this.active = active;
         Scene.requestDraw();
      } else {
         this.active = active;
      }
      this.active = this.mouseInside(event);
   }

   mouseUp(event: MouseEvent) {
      if (this.mouseInside(event)) {
         this.callback();
      }
      this.active = false;
      Scene.requestDraw();
   }

   draw() {
      if (this.active) {
         ctx.fillStyle = '#DDD';
         ctx.fillRect(this.x, this.y, this.width, this.height);
      }

      ctx.strokeStyle = '#BBB';
      ctx.lineWidth = 5;
      ctx.setLineDash([25, 25]);
      ctx.strokeRect(this.x, this.y, this.width, this.height);
      ctx.setLineDash([]);

      ctx.fillStyle = '#999';
      ctx.font = this.font;
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.fillText(this.text, this.x + this.width / 2, this.y + this.height / 2);
   }
}