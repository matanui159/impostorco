import ScriptLine from './ScriptLine';
import ctx from '../util/ctx';

namespace script {
   let art: {x: number, y: number}[] = [];
   for (let i = 0; i < 50; ++i) {
      let angle = Math.random() * Math.PI * 2;
      let radius = Math.random() + Math.random();
      if (radius > 1) {
         radius = 2 - radius;
      }
      art.push({
         x: 200 * radius * Math.cos(angle),
         y: 200 * radius * Math.sin(angle)
      });
   }

   export let lines: ScriptLine[] = [
      {line: 'Welcome to Imposter Co.'},
      {line: 'Here we make and sell fakes, duplicates... imposters as the name implies'},
      {line: 'Inventing new stuff is hard, ripping people off is easy... and profitable!'},
      {line: 'Your job here is to make these fakes, because you\'re the best of the best!'},
      {line: 'Well... the best who\'s fine with these apparent "bad business practices"'},
      {line: 'Anyway, let\'s get started'},
      {
         line: 'Replicate this ball',
         image: true,
         guide: () => {
            ctx.beginPath();
            ctx.arc(0, 0, 150, 0, Math.PI * 2);
            ctx.stroke();
         }
      },
      {line: 'Well...'},
      {line: 'I thought you would be good but...'},
      {line: 'This EXCEEDED MY EXPECTATIONS!'},
      {
         line: 'Let\s get you onto something else',
         image: false
      },
      {line: 'Here we go'},
      {
         line: 'Replicate this box',
         image: true,
         guide: () => {
            ctx.strokeRect(-150, -150, 300, 300);
         }
      },
      {line: 'Wow this is just amazing!'},
      {line: 'How can you be this good?'},
      {
         line: 'This next task should be easy for someone like you',
         image: false
      },
      {
         line: 'Replicate this abstract art',
         image: true,
         guide: () => {
            ctx.lineCap = 'round';
            ctx.lineJoin = 'round';
            ctx.beginPath();
            ctx.moveTo(0, 0);
            for (let point of art) {
               ctx.lineTo(point.x, point.y);
            }
            ctx.stroke();
         }
      },
      {
         line: 'That\'s not correct',
         dark: true
      },
      {
         line: 'Why is it not correct?',
         dark: true
      },
      {
         line: 'I thought you were better than this',
         dark: true
      },
      {line: 'Great job as always!'},
      {
         line: 'Why don\'t you take a break?',
         image: false
      },
      {line: ''},
      {line: 'Break\'s over'},
      {
         line: 'Replicate this flower',
         image: true,
         guide: () => {
            for (let angle = 0; angle < 2; angle += 0.4) {
               ctx.beginPath();
               ctx.arc(
                  50 * Math.cos((angle - 0.5) * Math.PI),
                  50 * Math.sin((angle - 0.5) * Math.PI) - 100,
                  30,
                  0, Math.PI * 2
               );
               ctx.stroke();
            }

            ctx.beginPath();
            ctx.moveTo(0, 100);
            ctx.lineTo(0, -100);
            ctx.stroke();

            ctx.beginPath();
            ctx.arc(0, -100, 40, 0, Math.PI * 2);
            ctx.fillStyle = '#FFF';
            ctx.fill();
            ctx.stroke();
         }
      },
      {line: 'Good job'},
      {
         line: 'Now... this house',
         image: true,
         guide: () => {
            ctx.strokeRect(-150, 0, 300, 150);
            ctx.strokeRect(-100, 50, 50, 100);
            ctx.strokeRect(50, 50, 50, 50);

            ctx.fillStyle = ctx.strokeStyle
            ctx.beginPath();
            ctx.arc(-60, 100, 5, 0, Math.PI * 2);
            ctx.fill();

            ctx.beginPath();
            ctx.moveTo(50, 75);
            ctx.lineTo(100, 75);
            ctx.moveTo(75, 50);
            ctx.lineTo(75, 100);
            ctx.stroke();
            
            ctx.beginPath();
            ctx.moveTo(-150, 0);
            ctx.lineTo(0, -150);
            ctx.lineTo(150, 0);
            ctx.stroke();
         }
      },
      {line: 'Doing well'},
      {
         line: 'Finally... I don\'t know... this dog?',
         image: true,
         guide: () => {
            // TODO: implement dog guide
         }
      },
      {
         line: 'This business can\'t afford for people to make mistakes',
         dark: true
      },
      {
         line: 'Why do you keep trying to destroy this business?',
         dark: true
      },
      {
         line: 'Are you even that good?',
         dark: true
      },
      {line: 'Yeah... goodjob'},
      {
         line: 'You don\'t look so good',
         image: false
      }
   ];

   let index = -1;
   export function next(): ScriptLine {
      return lines[++index];
   }
}
export default script;