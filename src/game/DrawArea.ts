import ctx from '../util/ctx';
import MouseArea from '../util/MouseArea';
import Scene from '../scene/Scene';

export default class DrawArea extends MouseArea {
   private ctx: CanvasRenderingContext2D;

   constructor(x: number, y: number, width: number, height: number) {
      super(x, y, width, height);

      let canvas = document.createElement('canvas');
      canvas.width = width;
      canvas.height = height;
      this.ctx = canvas.getContext('2d')!;
      this.ctx.lineWidth = 5;
      this.ctx.lineCap = 'round';
   }

   private line(x1: number, y1: number, x2: number, y2: number) {
      this.ctx.beginPath();
      this.ctx.moveTo(x1 - this.x, y1 - this.y);
      this.ctx.lineTo(x2 - this.x, y2 - this.y);
      this.ctx.stroke();
      Scene.requestDraw();
   }

   mouseDown(event: MouseEvent) {
      this.line(
         event.clientX,
         event.clientY,
         event.clientX,
         event.clientY
      );
   }

   mouseMove(event: MouseEvent) {
      this.line(
         event.clientX - event.movementX,
         event.clientY - event.movementY,
         event.clientX,
         event.clientY
      );
   }

   draw(dark: boolean) {
      ctx.strokeStyle = '#DDD';
      ctx.lineWidth = 5;
      ctx.setLineDash([50, 50]);
      ctx.strokeRect(this.x, this.y, this.width, this.height);
      ctx.setLineDash([]);

      if (dark) {
         ctx.shadowColor = '#FFF';
      } else {
         ctx.shadowColor = '#000';
      }
      
      ctx.shadowOffsetX = this.x + this.width;
      ctx.shadowBlur = 5;
      ctx.drawImage(this.ctx.canvas, -this.width, this.y);
      ctx.shadowBlur = 0;
      ctx.drawImage(this.ctx.canvas, -this.width, this.y);
      ctx.shadowOffsetX = 0;
   }
}