import Scene from "../scene/Scene";

export default interface ScriptLine {
   line: string;
   image?: boolean;
   dark?: boolean;
   guide?: () => void;
   scene?: Scene;
}