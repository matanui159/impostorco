export default class Scene {
   private static request = false;
   static current = new Scene();

   static requestDraw() {
      if (Scene.request) {
         console.warn('Redraw already requested');
      } else {
         Scene.request = true;
         requestAnimationFrame(() => {
            Scene.request = false;
            Scene.current.draw();
         });
      }
   }

   constructor() {
      Scene.requestDraw();
   }

   destroy() {}
   draw() {}
}