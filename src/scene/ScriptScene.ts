import Scene from './Scene';
import FadeScene from './FadeScene';
import ctx from '../util/ctx';
import font from '../util/font';
import ScriptLine from '../game/ScriptLine';
import script from '../game/script';
import DrawArea from '../game/DrawArea';
import Button from '../game/Button';

const DRAW_SIZE = 500;

export default class ScriptScene extends Scene {
   image?: DrawArea;
   private button?: Button;

   static next() {
      let fade = true;
      let line = script.next();
      if (line.dark) {
         fade = false;
      }
      if (Scene.current instanceof ScriptScene && Scene.current.script.dark) {
         fade = false;
      }

      let scene = new ScriptScene(line);
      if (fade) {
         Scene.current = new FadeScene(scene);
      } else {
         Scene.current = scene;
      }
   }

   constructor(public script: ScriptLine) {
      super();
      if (typeof script.image === 'undefined') {
         this.image = (Scene.current as ScriptScene).image;
      } else if (script.image) {
         this.image = new DrawArea(0, 0, DRAW_SIZE, DRAW_SIZE);
         this.button = new Button(0, 0, 'Done ->', () => {
            ScriptScene.next();
         });
      }

      if (!script.image) {
         // TODO: use audio end instead of timeout
         setTimeout(() => {
            ScriptScene.next();
         }, 2000);
      }

      script.scene = this;
      Scene.current.destroy();
   }

   destroy() {
      if (typeof this.image !== 'undefined') {
         this.image.destroy();
      }
      if (typeof this.button !== 'undefined') {
         this.button.destroy();
      }
   }

   draw() {
      let width = ctx.canvas.width;
      let height = ctx.canvas.height;
      let header = (height - DRAW_SIZE) / 2;

      if (this.script.dark) {
         ctx.fillStyle = '#000';
      } else {
         ctx.fillStyle = '#FFF';
      }
      ctx.fillRect(0, 0, width, height);

      ctx.fillStyle = '#999';
      ctx.font = `50px ${font.name}`;
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      let text_width = ctx.measureText(this.script.line).width;
      if (text_width > 1000) {
         ctx.font = `30px ${font.name}`;
      }
      ctx.fillText(this.script.line, width / 2, header / 2, 1000);

      if (typeof this.script.guide !== 'undefined') {
         ctx.strokeStyle = '#BBB';
         ctx.lineWidth = 5;
         ctx.save();
         ctx.translate(width / 2, height / 2);
         this.script.guide();
         ctx.restore();
      }

      if (typeof this.image !== 'undefined') {
         this.image.x = (width - DRAW_SIZE) / 2;
         this.image.y = header;
         this.image.draw(!!this.script.dark);

         if (typeof this.button !== 'undefined') {
            this.button.x = this.image.x + this.image.width + 25;
            this.button.y = this.image.y + this.image.height - this.button.height;
            this.button.draw();
         }
      }
   }
}