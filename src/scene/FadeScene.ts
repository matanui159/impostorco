import Scene from './Scene';
import ctx from '../util/ctx';

export default class FadeScene extends Scene {
   private ctx: CanvasRenderingContext2D;
   private start = Date.now();

   constructor(private next: Scene) {
      super();
      let canvas = document.createElement('canvas');
      canvas.width = ctx.canvas.width;
      canvas.height = ctx.canvas.height;
      this.ctx = canvas.getContext('2d')!;
      this.ctx.drawImage(ctx.canvas, 0, 0);
   }

   draw() {
      let time = (Date.now() - this.start) / 500;
      this.next.draw();
      
      var alpha = 1 - time;
      if (alpha < 0) {
         alpha = 0;
      }
      ctx.globalAlpha = alpha;
      ctx.drawImage(this.ctx.canvas, 0, 0);
      ctx.globalAlpha = 1;

      if (time >= 1) {
         Scene.current = this.next;
      }
      Scene.requestDraw();
   }
}