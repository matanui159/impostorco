import font from 'util/font';
import ScriptScene from 'scene/ScriptScene';

font.load('Gloria Hallelujah', () => {
   ScriptScene.next();
});