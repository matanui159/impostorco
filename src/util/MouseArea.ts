export default class MouseArea {
   constructor(
      public x: number, public y: number,
      public width: number, public height: number
   ) {
      areas.push(this);
      console.info(`Added mouse area, ${areas.length} now exist`);
   }

   destroy() {
      let index = areas.indexOf(this);
      if (index !== -1) {
         areas.splice(index, 1);
         console.info(`Removed mouse area, ${areas.length} now exist`);
      }
   }

   mouseInside(event: MouseEvent): boolean {
      if (event.clientX >= this.x && event.clientX <= this.x + this.width) {
         if (event.clientY >= this.y && event.clientY <= this.y + this.height) {
            return true;
         }
      }
      return false;
   }

   mouseDown(event: MouseEvent) {}
   mouseMove(event: MouseEvent) {}
   mouseUp(event: MouseEvent) {}
}

let areas: MouseArea[] = [];
let current: MouseArea | null = null;

window.addEventListener('mousedown', event => {
   for (let area of areas) {
      if (area.mouseInside(event)) {
         current = area;
         current.mouseDown(event);
      }
   }
});

window.addEventListener('mousemove', event => {
   if (current !== null) {
      current.mouseMove(event);
   }
});

window.addEventListener('mouseup', event => {
   if (current !== null) {
      current.mouseUp(event);
   }
   current = null;
});