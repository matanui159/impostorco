declare namespace WebFont {
   function load(config: {
      google: {
         families: string[]
      },
      active: () => void
   }): void;
}

namespace font {
   export let name = 'serif';

   export function load(font_name: string, callback: () => void) {
      let script = document.createElement('script');
      script.addEventListener('load', () => {
         WebFont.load({
            google: {
               families: [font_name]
            },
            active: () => {
               name = font_name;
               callback();
            }
         });
      });
      script.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
      document.body.appendChild(script);
   }
}
export default font;