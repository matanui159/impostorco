import Scene from '../scene/Scene';

let canvas = document.getElementsByTagName('canvas')[0];
let ctx = canvas.getContext('2d')!;
if (ctx === null) {
   throw new Error('Failed to create context');
}

function resize() {
   canvas.width = window.innerWidth;
   canvas.height = window.innerHeight;
   Scene.requestDraw();
}
resize();
window.addEventListener('resize', resize);

export default ctx;